\LoadClass{standalone}

\ProvidesClass{iditastretch}[2018/06/16 Class for Iditacard stretches]

\RequirePackage{fontspec}
\RequirePackage{xcolor}

\RequirePackage[none]{hyphenat}

\RequirePackage{tikz}
\usetikzlibrary{positioning,shapes,shadows,arrows,backgrounds,fit}
\RequirePackage{shapepar}
\RequirePackage{microtype}

\graphicspath{{images/}}

\newfontfamily\bebas{Bebas Neue Regular}
\newfontfamily\alegreya{Alegreya}

\definecolor{stage_one}{HTML}{7FDF7D}
\definecolor{stage_two}{HTML}{9FB76F}
\definecolor{stage_three}{HTML}{5F5FFF}
\definecolor{stage_four}{HTML}{3FDFDF}
\definecolor{stage_five}{HTML}{DFDF3F}
\definecolor{stage_six}{HTML}{9F9F9F}
\definecolor{stage_seven}{HTML}{FF5F5F}
\definecolor{stage_eight}{HTML}{CF5FCF}

\definecolor{energy}{HTML}{003FFF}
\definecolor{health}{HTML}{FF0000}
\definecolor{risk}{HTML}{000000}

\definecolor{utility}{HTML}{FFFFFF}
\definecolor{attachment}{HTML}{5FAFCF}
\definecolor{dog}{HTML}{BF9F7F}
\definecolor{stretch}{HTML}{9F9F9F}
\definecolor{personal}{HTML}{AF5FCF}
\definecolor{movement}{HTML}{7F7FDF}
\definecolor{damage}{HTML}{FF6F6F}
\definecolor{sled}{HTML}{7FDF7F}
\definecolor{food}{HTML}{CFCF4F}

\newcommand{\Mm}{Mm}
\newcommand{\Mms}{Mms}
\newcommand{\move}[1]{\raisebox{-0.2em}{\includegraphics[height=1em]{icons/dog-sled-icon.png}} #1}
\newcommand{\destroy}[2]{\raisebox{-0.1em}{\includegraphics[height=1em]{icons/card-pickup.png}} #2 \raisebox{-0.1em}{\includegraphics[height=1em]{icons/trash-can.png}} #1}
\newcommand{\take}[2]{\raisebox{-0.1em}{\includegraphics[height=1em]{icons/backpack.png}} #2 \raisebox{-0.1em}{\includegraphics[height=1em]{icons/shopping-cart.png}} #1}
\newcommand{\draw}[1]{\raisebox{-0.1em}{\includegraphics[height=1em]{icons/card-draw.png}} #1}
\newcommand{\daydraw}[1]{\raisebox{-0.1em}{\includegraphics[height=1em]{icons/card-plus.png}} #1 \includegraphics[height=1em]{icons/solar-time.png}}
\newcommand{\personal}{{\color{personal}personal}}
\newcommand{\hypothermia}{\raisebox{-0.1em}{\includegraphics[height=1em]{icons/hypo.png}}}
\newcommand{\starvation}{\raisebox{-0.1em}{\includegraphics[height=1em]{icons/starve.png}}}
\newcommand{\speed}[1]{\raisebox{-0.1em}{\includegraphics[height=1em]{icons/speed.png}}#1}

\newcommand{\difficulty}[2]{%
    \newcommand{\easybackgroundcolor}{#1}
    \newcommand{\hardbackgroundcolor}{#2}
}

\newcommand{\name}[2]{%
    \node [rectangle, anchor=north, minimum width=1in, minimum height=1.0/3.0in, text centered, text width=1in, inner sep=0mm] at (200.0/300.0,450.0/300.0) {\bf\fontsize{16}{18}\bebas#1\par};
    \node [rectangle, anchor=north, minimum width=1in, minimum height=1.0/3.0in, text centered, text width=1in, inner sep=0mm] at (550.0/300.0,450.0/300.0) {\bf\fontsize{16}{18}\bebas#2\par};
}

\newcommand{\effect}[2]{%
    \node [rectangle, anchor=north, minimum width=1in, minimum height=1.0/3.0in, text centered, text width=1in, inner sep=0mm] at (200.0/300.0,175.0/300.0) {\bf\fontsize{10}{10}\bebas#1\par};
    \node [rectangle, anchor=north, minimum width=1in, minimum height=1.0/3.0in, text centered, text width=1in, inner sep=0mm] at (550.0/300.0,175.0/300.0) {\bf\fontsize{10}{10}\bebas#2\par};
}

% Aspect ratio of about 3:2
\newcommand{\art}[2]{%
    \node [rectangle, minimum width=0.9in, minimum height=200.0/300.0, inner sep=0, anchor=north west] at (65.0/300.0,390.0/300.0) {\noindent\includegraphics*[width=0.9in, height=0.66666in]{#1}};
    \node [rectangle, minimum width=0.9in, minimum height=200.0/300.0, inner sep=0, anchor=north west] at (415.0/300.0,390.0/300.0) {\noindent\includegraphics*[width=0.9in, height=0.66666in]{#2}};
}

\newcommand{\easyhypo}[1]{%
    % TODO: Improve alignment
    \ifnum 0<#1
        %\fill [left color=energy, right color=personal] (50.0/300.0,375.0/300.0) rectangle (65.0/300.0,325.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (50.0/300.0,360.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
    \ifnum 1<#1
        %\fill [left color=energy, right color=personal] (50.0/300.0,315.0/300.0) rectangle (65.0/300.0,265.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (50.0/300.0,290.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
    \ifnum 2<#1
        %\fill [left color=energy, right color=personal] (50.0/300.0,255.0/300.0) rectangle (65.0/300.0,205.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (50.0/300.0,220.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
}

\newcommand{\easystarve}[1]{%
    % TODO: Improve alignment
    \ifnum 0<#1
        %\fill [right color=health, left color=food] (336.0/300.0,375.0/300.0) rectangle (350.0/300.0,325.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (350.0/300.0,360.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
    \ifnum 1<#1
        %\fill [right color=health, left color=food] (336.0/300.0,315.0/300.0) rectangle (350.0/300.0,265.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (350.0/300.0,290.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
    \ifnum 2<#1
        %\fill [right color=health, left color=food] (336.0/300.0,255.0/300.0) rectangle (350.0/300.0,205.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (350.0/300.0,220.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
}

\newcommand{\hardhypo}[1]{%
    % TODO: Improve alignment
    \ifnum 0<#1
        %\fill [left color=energy, right color=personal] (400.0/300.0,375.0/300.0) rectangle (415.0/300.0,325.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (400.0/300.0,360.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
    \ifnum 1<#1
        %\fill [left color=energy, right color=personal] (400.0/300.0,315.0/300.0) rectangle (415.0/300.0,265.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (400.0/300.0,290.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
    \ifnum 2<#1
        %\fill [left color=energy, right color=personal] (400.0/300.0,255.0/300.0) rectangle (415.0/300.0,205.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (400.0/300.0,220.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
}

\newcommand{\hardstarve}[1]{%
    % TODO: Improve alignment
    \ifnum 0<#1
        %\fill [right color=health, left color=food] (686.0/300.0,375.0/300.0) rectangle (700.0/300.0,325.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (700.0/300.0,360.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
    \ifnum 1<#1
        %\fill [right color=health, left color=food] (686.0/300.0,315.0/300.0) rectangle (700.0/300.0,265.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (700.0/300.0,290.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
    \ifnum 2<#1
        %\fill [right color=health, left color=food] (686.0/300.0,255.0/300.0) rectangle (700.0/300.0,205.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (700.0/300.0,220.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
}

\newcommand{\flava}[2]{%
    \node [rectangle, minimum width=1in, minimum height=50.0/300.0, text centered, text width=1in, inner sep=0mm, anchor=north] at (200.0/300.0,75.0/300.0) {\fontsize{4}{6}\alegreya"#1"\par};
    \node [rectangle, minimum width=1in, minimum height=50.0/300.0, text centered, text width=1in, inner sep=0mm, anchor=north] at (550.0/300.0,75.0/300.0) {\fontsize{4}{6}\alegreya"#2"\par};
}

\newenvironment{cardstretch}
{% Begin
\noindent\begin{tikzpicture}[x=1in,y=1in]
\fill [left color=\hardbackgroundcolor, right color=\easybackgroundcolor] (0,0) rectangle (2.5,1.75);
%\fill [\hardbackgroundcolor] (0,0) rectangle (1.25,1.75);
%\fill [\easybackgroundcolor] (1.25,0) rectangle (2.5,1.75);
\fill[rounded corners=10pt] [\easybackgroundcolor] (50.0/300.0,50.0/300.0) rectangle (350.0/300.0,475.0/300.0);
\fill[rounded corners=10pt] [\hardbackgroundcolor] (400.0/300.0,50.0/300.0) rectangle (700.0/300.0,475.0/300.0);
}
{% End
\end{tikzpicture}}
